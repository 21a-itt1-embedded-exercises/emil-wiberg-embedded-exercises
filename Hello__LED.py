Your first physical computing program: Hello, LED!
Just as printing ‘Hello, World’ to the screen is a fantastic first step in learning a programming
language, making an LED light up is the traditional introduction to learning physical computing.
You can get started without any additional components, too: your Raspberry Pi Pico has a small
LED, known as a surface-mount device (SMD) LED, on top.

from machine import Pin
import machine
import time

led = machine.Pin(15, machine.Pin.OUT)
button = machine.Pin(14, machine.Pin.IN, machine.Pin.PULL_DOWN)

while True:
    if button.value():
        led.toggle()
        time.sleep(0.5)


